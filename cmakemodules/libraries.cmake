SET(VEGAFEM_LIBRARIES
    configFile
    openGLHelper    
    lighting
    imageIO
    massSpringSystem 
    sparseSolver
    integratorSparse
    integrator                  
    clothBW                   
    mesher             
    getopts                                
    sceneObject       
    sparseMatrix                
    stencilForceModel
    volumetricMesh
    forceModel
    constrainedDOFs
    objMesh
    graph
    mesh
    distanceField
    basicAlgorithms
)

SET(VEGAFEM_LINK_DIRECTORIES $ENV{VegaFEM}/libraries/lib)

LIBRARIES(${CLOTH_PLUGIN_NAME} ${VEGAFEM_LIBRARIES})
LINK_DIRS(${CLOTH_PLUGIN_NAME} ${VEGAFEM_LINK_DIRECTORIES})
MACOS_FRAMEWORKS(${CLOTH_PLUGIN_NAME}
    OpenGL
    GLUT
)