SET(PLUGIN_SOURCES
    src/plugin/PluginParameter.h
    src/plugin/PluginParameter.cpp

    src/plugin/PluginParameterRange.h
    src/plugin/PluginParameterRange.cpp

    src/plugin/PluginRangedParameter.h
    src/plugin/PluginRangedParameter.cpp

    src/plugin/PluginParameters.h
    src/plugin/PluginParameters.cpp

    src/plugin/BaseSOP.h
    src/plugin/BaseSOP.cpp
)

SET(MATH_SOURCES
    src/math/Vector3d.h
    src/math/Vector3d.cpp
)

SET(SIMULATION_SOURCES
    src/simulation/ClothSimulation.h
    src/simulation/ClothSimulation.cpp
)

SOURCES(${CLOTH_PLUGIN_NAME}
    ${PLUGIN_SOURCES}
    ${MATH_SOURCES}
    ${SIMULATION_SOURCES}

    src/plugin/cloth/Cloth.h
    src/plugin/cloth/Cloth.cpp
)