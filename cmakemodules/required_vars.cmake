IF(NOT DEFINED ENV{HFS})
    MESSAGE(FATAL_ERROR
    "
        You have to define the HFS environment variable before runnning CMake.
        Example how to do this: cd /opt/hfs16.5.571 && source houdini_setup
        Or, if you are using CLion, you can define it in your CMake profile:
        https://www.jetbrains.com/help/clion/configuring-cmake.html
    ")
ENDIF()

IF(NOT DEFINED ENV{VegaFEM})
    MESSAGE(FATAL_ERROR "VegaFEM env variable is required. Please, set env variable $VegaFEM value to path to Vega FEM")
ENDIF()