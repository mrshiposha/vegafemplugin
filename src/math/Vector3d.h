#ifndef MATH_VECTOR3D_H
#define MATH_VECTOR3D_H

#include <cmath>

class Vector3d
{
public:
    Vector3d();

    Vector3d(double x, double y, double z);

    Vector3d(const Vector3d &) = default;

    Vector3d(Vector3d &&) = default;

    ~Vector3d() = default;

    Vector3d &operator=(const Vector3d &) = default;

    Vector3d &operator=(Vector3d &&) = default;

    void operator+=(const Vector3d &);

    void operator-=(const Vector3d &);

    Vector3d operator+(const Vector3d &) const;

    Vector3d operator-(const Vector3d &) const;

    Vector3d operator+() const;

    Vector3d operator-() const;

    void operator*=(double);
    
    Vector3d operator*(double) const;

    void operator/=(double);

    Vector3d operator/(double) const;

    double dot(const Vector3d &) const;


    double get_x() const;

    double x() const;

    double &x();

    void set_x(double);

    Vector3d &x(double);


    double get_y() const;

    double y() const;

    double &y();

    void set_y(double);

    Vector3d &y(double);


    double get_z() const;

    double z() const;

    double &z();

    void set_z(double);

    Vector3d &z(double);

private:
    double _x, _y, _z;
};

#endif // MATH_VECTOR3D_H