#include "Vector3d.h"

Vector3d::Vector3d()
: Vector3d(0., 0., 0.)
{}

Vector3d::Vector3d(double x, double y, double z)
: _x(x), _y(y), _z(z)
{}

void Vector3d::operator+=(const Vector3d &rhs)
{
    _x += rhs._x;
    _y += rhs._y;
    _z += rhs._z;
}

void Vector3d::operator-=(const Vector3d &rhs)
{
    _x -= rhs._x;
    _y -= rhs._y;
    _z -= rhs._z;
}

Vector3d Vector3d::operator+(const Vector3d &rhs) const
{
    Vector3d copy(*this);
    copy += rhs;

    return copy;
}

Vector3d Vector3d::operator-(const Vector3d &rhs) const
{
    Vector3d copy(*this);
    copy -= rhs;

    return copy;
}

Vector3d Vector3d::operator+() const
{
    return *this;
}

Vector3d Vector3d::operator-() const
{
    Vector3d copy(*this);

    copy._x = -copy._x;
    copy._y = -copy._y;
    copy._z = -copy._z;

    return copy;
}

void Vector3d::operator*=(double value)
{
    _x *= value;
    _y *= value;
    _z *= value;
}

Vector3d Vector3d::operator*(double value) const
{
    Vector3d copy(*this);
    copy *= value;

    return copy;
}

void Vector3d::operator/=(double value)
{
    _x /= value;
    _y /= value;
    _z /= value;
}

Vector3d Vector3d::operator/(double value) const
{
    Vector3d copy(*this);
    copy /= value;

    return copy;
}

double Vector3d::dot(const Vector3d &other) const
{
    return _x*other._x + _y*other._y + _z*other._z;
}


double Vector3d::get_x() const
{
    return _x;
}

double Vector3d::x() const
{
    return get_x();
}

double &Vector3d::x()
{
    return _x;
}

void Vector3d::set_x(double x)
{
    _x = x;
}

Vector3d &Vector3d::x(double x)
{
    set_x(x);
    return *this;
}


double Vector3d::get_y() const
{
    return _y;
}

double Vector3d::y() const
{
    return get_y();
}

double &Vector3d::y()
{
    return _y;
}

void Vector3d::set_y(double y)
{
    _y = y;
}

Vector3d &Vector3d::y(double y)
{
    set_y(y);
    return *this;
}


double Vector3d::get_z() const
{
    return _z;
}

double Vector3d::z() const
{
    return get_z();
}

double &Vector3d::z()
{
    return _z;
}

void Vector3d::set_z(double z)
{
    _z = z;
}

Vector3d &Vector3d::z(double z)
{
    set_z(z);
    return *this;
}