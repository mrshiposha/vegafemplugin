#ifndef PLUGIN_PARAMETER_RANGE_H
#define PLUGIN_PARAMETER_RANGE_H

#include <PRM/PRM_Include.h>

class PluginParameterRange
{
public:
    class Value 
    {
    public:
        Value(fpreal value, PRM_RangeFlag);

        Value(const Value &) = default;

        Value(Value &&) = default;

        ~Value() = default;

        Value &operator=(const Value &) = delete;

        Value &operator=(Value &&) = delete;

        fpreal get() const;

        PRM_RangeFlag flag() const;

        operator fpreal() const;

    private:
        const fpreal        _value;
        const PRM_RangeFlag _flag;
    };

    PluginParameterRange(const Value &min, const Value &max);

    PluginParameterRange(const PluginParameterRange &) = default;

    PluginParameterRange(PluginParameterRange &&) = default;

    ~PluginParameterRange() = default;

    PluginParameterRange &operator=(const PluginParameterRange &) = delete;

    PluginParameterRange &operator=(PluginParameterRange &&) = delete;

    const Value &min() const;

    const Value &max() const;

private:
    const Value _min, _max;
};

PluginParameterRange range(const PluginParameterRange::Value &min, const PluginParameterRange::Value &max);

PluginParameterRange::Value free(fpreal);

PluginParameterRange::Value ui(fpreal);

PluginParameterRange::Value prm(fpreal);

PluginParameterRange::Value restricted(fpreal); 

#endif // PLUGIN_PARAMETER_RANGE_H