#include "BaseSOP.h"

BaseSOP::BaseSOP(OP_Network *net, const char *name, OP_Operator *entry)
: SOP_Node(net, name, entry), is_objmesh_changed(false), objmesh(nullptr)
{}

bool BaseSOP::is_objmesh_loaded() const
{
    return objmesh != nullptr;
}

bool BaseSOP::is_objmesh_input_changed() const
{
    return is_objmesh_changed;
}

void BaseSOP::reload_objmesh()
{
    std::vector<double> vertices;
    std::vector<int>    face_vertex_count;
    std::vector<int>    faces;

    GA_RWHandleV3 ph(gdp->getP());
    GA_Offset ptoff;
    UT_Vector3 pos;
    GA_FOR_ALL_PTOFF(gdp, ptoff)
    {
        pos = ph.get(ptoff);
        vertices.push_back(pos[0]);
        vertices.push_back(pos[1]);
        vertices.push_back(pos[2]);
    }

    GEO_Primitive *prim;
    size_t faces_count = 0;
    size_t vertex_count = 0;
    GA_FOR_ALL_PRIMITIVES(gdp, prim)
    {
        vertex_count = prim->getVertexCount();

        face_vertex_count.push_back(vertex_count);

        for(size_t i = 0; i < vertex_count; ++i)
            faces.push_back(prim->getPointIndex(i));

        ++faces_count;
    }

    vertex_count = vertices.size() / 3;
    objmesh = std::make_shared<ObjMesh>
    (
        vertex_count, 
        vertices.data(), 
        faces_count, 
        face_vertex_count.data(), 
        faces.data()
    );

    is_objmesh_changed = false;
}

std::shared_ptr<ObjMesh> BaseSOP::get_objmesh()
{
    if(is_objmesh_loaded() && !is_objmesh_changed)
        return objmesh;
    else
        reload_objmesh();

    return objmesh;
}

void BaseSOP::set_node_error(const std::string &message)
{
    addError(SOP_MESSAGE, message.c_str());
}

void BaseSOP::opChanged(OP_EventType reason, void *data)
{
    SOP_Node::opChanged(reason, data);

    if(reason == OP_INPUT_REWIRED)
        is_objmesh_changed = true;
}