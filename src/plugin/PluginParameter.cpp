#include "PluginParameter.h"

PluginParameter::PluginParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label
): prm_type(type), _token(token), _label(label), prm_default(nullptr),
   prm_name(token.c_str(), label.c_str()),
   callback(nullptr)
{}

PluginParameter::PluginParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    fpreal default_value
): prm_type(type), _token(token), _label(label), prm_default(std::make_shared<PRM_Default>(default_value)),
   prm_name(_token.c_str(), _label.c_str()),
   callback(nullptr)
{}

PluginParameter::PluginParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    Callback callback
): prm_type(type), _token(token), _label(label), prm_default(nullptr),
   prm_name(_token.c_str(), _label.c_str()),
   callback(callback)
{}

PluginParameter::PluginParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    fpreal default_value,
    Callback callback
): prm_type(type), _token(token), _label(label), prm_default(std::make_shared<PRM_Default>(default_value)),
   prm_name(_token.c_str(), _label.c_str()),
   callback(callback)
{}

PluginParameter::~PluginParameter()
{}

const PRM_Type &PluginParameter::type() const
{
    return prm_type;
}

const std::string &PluginParameter::token() const
{
    return _token;
}

const std::string &PluginParameter::label() const
{
    return _label;
}

bool PluginParameter::has_default_value() const
{
    return prm_default == nullptr;
}

const PRM_Default &PluginParameter::default_value() const throw(PluginParameterNoDefaultValue)
{
    if(!has_default_value())
        throw PluginParameterNoDefaultValue();
    return *prm_default;
}

const std::shared_ptr<PRM_Default> PluginParameter::default_value_ptr() const
{
    return prm_default;
}

PRM_Template PluginParameter::build_prm_template()
{
    return PRM_Template(prm_type, 1, &prm_name, prm_default.get(), nullptr, nullptr, callback);
}

PluginParameter::operator PRM_Template()
{
    return build_prm_template();
}

fpreal PluginParameter::get_float_value(SOP_Node *node, Time time, const PRM_Template *prm_template)
{
    return node->evalFloat(prm_template->getNamePtr()->getToken(), 0, time);
}

int PluginParameter::get_int_value(SOP_Node *node, Time time, const PRM_Template *prm_template)
{
    return node->evalInt(prm_template->getNamePtr()->getToken(), 0, time);
}

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label
)
{
    return std::make_shared<PluginParameter>(type, token, label);
}

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    fpreal default_value
)
{
    return std::make_shared<PluginParameter>(type, token, label, default_value);
}

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    PluginParameter::Callback callback
)
{
    return std::make_shared<PluginParameter>(type, token, label, callback);
}

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    fpreal default_value,
    PluginParameter::Callback callback
)
{
    return std::make_shared<PluginParameter>(type, token, label, default_value, callback);
}