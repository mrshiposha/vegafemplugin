#ifndef PLUGIN_PARAMETERS_H
#define PLUGIN_PARAMETERS_H

#include <vector>
#include <memory>

#include "PluginParameter.h"
#include "PluginRangedParameter.h"

class PluginParameters
{
public:
    PluginParameters();

    PluginParameters(const std::vector<std::shared_ptr<PluginParameter>> &);

    PluginParameters(std::initializer_list<std::shared_ptr<PluginParameter>>);

    PluginParameters(const PluginParameters &) = default;

    PluginParameters(PluginParameters &&) = default;

    ~PluginParameters() = default;

    PluginParameters &operator=(const PluginParameters &) = default;

    PluginParameters &operator=(PluginParameters &&) = default;

    PluginParameters &add(std::shared_ptr<PluginParameter>);

    PRM_Template *data();

    operator PRM_Template*();

private:
    std::vector<std::shared_ptr<PluginParameter>> parameters;
    std::vector<PRM_Template>                     prm_templates;
};

#endif // PLUGIN_PARAMETERS_H