#include "PluginRangedParameter.h"

PluginRangedParameter::PluginRangedParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range
): PluginParameter(type, token, label), _range(range), 
_prm_range(_range.min().flag(), _range.min(), _range.max().flag(), _range.max())
{}

PluginRangedParameter::PluginRangedParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    fpreal default_value
): PluginParameter(type, token, label, default_value), _range(range),
_prm_range(_range.min().flag(), _range.min(), _range.max().flag(), _range.max())
{}

PluginRangedParameter::PluginRangedParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    Callback callback
): PluginParameter(type, token, label, callback), _range(range),
_prm_range(_range.min().flag(), _range.min(), _range.max().flag(), _range.max())
{}

PluginRangedParameter::PluginRangedParameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    fpreal default_value,
    Callback callback
): PluginParameter(type, token, label, default_value, callback), _range(range),
_prm_range(_range.min().flag(), _range.min(), _range.max().flag(), _range.max())
{}

const PluginParameterRange &PluginRangedParameter::range() const
{
    return _range;
}

PRM_Template PluginRangedParameter::build_prm_template()
{
    return PRM_Template(type(), 1, &this->prm_name, default_value_ptr().get(), nullptr, &_prm_range, callback);
}

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range
)
{
    return std::make_shared<PluginRangedParameter>(type, token, label, range);
}

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    fpreal default_value
)
{
    return std::make_shared<PluginRangedParameter>(type, token, label, range, default_value);
}

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    PluginParameter::Callback callback
)
{
    return std::make_shared<PluginRangedParameter>(type, token, label, range, callback);    
}

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &type,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    fpreal default_value,
    PluginParameter::Callback callback
)
{
    return std::make_shared<PluginRangedParameter>(type, token, label, range, default_value, callback);    
}