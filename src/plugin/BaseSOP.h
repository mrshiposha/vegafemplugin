#ifndef PLUGIN_BASE_SOP_H
#define PLUGIN_BASE_SOP_H

#include <SOP/SOP_Node.h>
#include <objMesh.h>


class BaseSOP : public SOP_Node
{
public:
    BaseSOP(OP_Network *net, const char *name, OP_Operator *entry);

    bool is_objmesh_loaded() const;

    bool is_objmesh_input_changed() const;

    void reload_objmesh();

    std::shared_ptr<ObjMesh> get_objmesh();

    void set_node_error(const std::string &);

protected:
    virtual void opChanged(OP_EventType reason, void *data) override;

private:
    bool is_objmesh_changed;
    std::shared_ptr<ObjMesh> objmesh;
};

#endif // PLUGIN_BASE_SOP_H