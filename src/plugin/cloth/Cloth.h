#ifndef CLOTH_PLUGIN_H
#define CLOTH_PLUGIN_H

#include <vector>

#include "plugin/BaseSOP.h"
#include "simulation/ClothSimulation.h"

class Cloth : public BaseSOP
{
    friend void newSopOperator(OP_OperatorTable *);    
public:
    static OP_Node	*hdk_constructor(OP_Network *net, const char *name, OP_Operator *entry);

    Cloth(OP_Network *net, const char *name, OP_Operator *entry);

    void load_fixed_vertices();

    void reset();

    std::shared_ptr<ClothSimulation> simulation();

protected:
    virtual OP_ERROR cookMySop(OP_Context &context) override;

    virtual void opChanged(OP_EventType reason, void *data) override;

private:
    void apply_cloth_deformations();

    void initialize_simulation();

    void reset_simulation();

    std::shared_ptr<ClothSimulation> _simulation;
    fpreal                           last_frame;
};

#endif // VEGAFEM_CLOTH_PLUGIN_H