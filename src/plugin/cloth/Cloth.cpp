#include <GU/GU_Detail.h>
#include <GA/GA_Handle.h>
#include <OP/OP_AutoLockInputs.h>
#include <OP/OP_Director.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <PRM/PRM_Include.h>
#include <UT/UT_DSOVersion.h>

#include "Cloth.h"

#include "plugin/PluginParameters.h"

#include <fstream>

void newSopOperator(OP_OperatorTable *table)
{
    static PluginParameters parameters = 
    {
        ranged_parameter
        (
            PRM_FLT_J,
            "time_step", "Time step",
            range(restricted(0.00001), ui(10)),
            0.01,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_timestep(cloth->evalFloat("time_step", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_INT,
            "sub_steps", "Substeps",
            range(restricted(1), ui(25)),
            3,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_substeps(cloth->evalFloat("sub_steps", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "damping_mass", "Damping mass",
            range(restricted(0.0), ui(5)),
            0.0,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_damping_mass(cloth->evalFloat("damping_mass", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "damping_stiffness", "Damping stiffness",
            range(restricted(0.00001), ui(10)),
            0.01,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_damping_stiffness(cloth->evalFloat("damping_stiffness", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "surface_density", "Surface density",
            range(restricted(0.0), ui(1)),
            0.25,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_surface_density(cloth->evalFloat("surface_density", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "tensile_stiffness", "Tensile stiffness",
            range(restricted(0.0), ui(12000)),
            4500.0,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_material_tensile_stiffness(cloth->evalFloat("tensile_stiffness", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "shear_stiffness", "Shear stiffness",
            range(restricted(0.0), ui(500)),
            100.0,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_material_shear_stiffness(cloth->evalFloat("shear_stiffness", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "bend_stiffness_u", "Bend stiffness U",
            range(restricted(0.0), ui(0.1)),
            0.0002,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_material_bend_stiffness_u(cloth->evalFloat("bend_stiffness_u", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "bend_stiffness_v", "Bend stiffness V",
            range(restricted(0.0), ui(0.1)),
            0.0002,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_material_bend_stiffness_v(cloth->evalFloat("bend_stiffness_v", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "material_damping", "Material damping",
            range(restricted(0.0), ui(0.1)),
            0.0,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_material_damping(cloth->evalFloat("material_damping", 0, time));

                return 0;
            }
        ),

        parameter
        (
            PRM_TOGGLE,
            "use_rest_angles", "Use rest angles",
            false,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_rest_angles_usage(cloth->evalInt("use_rest_angles", 0, time));

                return 0;
            }
        ),

        parameter
        (
            PRM_TOGGLE,
            "add_gravity_to_elastic_forces", "Add gravity to elastic forces",
            true,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_adding_gravity_to_elastic_forces(cloth->evalInt("add_gravity_to_elastic_forces", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "g", "g",
            range(restricted(0.0), ui(100.0)),
            9.81,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_g(cloth->evalFloat("g", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_FLT_J,
            "solver_epsilon", "Solver epsilon",
            range(restricted(0.000000001), ui(0.1)),
            1E-5,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_epsilon(cloth->evalFloat("solver_epsilon", 0, time));

                return 0;
            }
        ),

        ranged_parameter
        (
            PRM_INT,
            "max_solver_iterations", "Max solver iterations",
            range(restricted(1), ui(5)),
            1,
            [](PluginParameter::Node node, PluginParameter::Index, PluginParameter::Time time, const PRM_Template *)
            {
                auto cloth = static_cast<Cloth*>(node);
                cloth->simulation()->set_max_iterations(cloth->evalInt("max_solver_iterations", 0, time));

                return 0;
            }
        )
    };

    table->addOperator
    (
        new OP_Operator
        (
            PLUGIN_NAME,
            PLUGIN_NAME,
            Cloth::hdk_constructor,
            parameters,
            1,
            1
        )
    );
}

OP_Node	*Cloth::hdk_constructor(OP_Network *net, const char *name, OP_Operator *entry)
{
    return new Cloth(net, name, entry);
}

Cloth::Cloth(OP_Network *net, const char *name, OP_Operator *entry)
: BaseSOP(net, name, entry), _simulation(nullptr), last_frame(0)
{}

void Cloth::load_fixed_vertices()
{
    GA_Attribute *attr = gdp->findAttribute(GA_ATTRIB_POINT, "fixed");
    
    if(attr == nullptr)
        return;
    
    int tuple_size = attr->getTupleSize();
    UT_DeepString attr_name = attr->getExportName();
    GA_StorageClass attr_storage_class = attr->getStorageClass();

    if(attr_storage_class != GA_STORECLASS_INT)
        return;

    GA_Offset ptoff;
    size_t i = 0;

    _simulation->unfix_all_vertices();
    GA_FOR_ALL_PTOFF(gdp, ptoff)
    {
        GA_ROAttributeRef attr_ref(gdp->findIntTuple(GA_ATTRIB_POINT, attr_name, tuple_size));
        GA_ROHandleI handle(attr);

        if(attr_ref.isValid())
        {
            if(handle.get(ptoff, 0))
                _simulation->fix_vertex(i);
        }

        ++i;
    }
}

void Cloth::reset()
{
    reload_objmesh();
    initialize_simulation();
}

std::shared_ptr<ClothSimulation> Cloth::simulation()
{
    return _simulation;
}

OP_ERROR Cloth::cookMySop(OP_Context &context) try
{
    OP_AutoLockInputs inputs(this);
    if (inputs.lock(context) >= UT_ERROR_ABORT)
        return error();

    fpreal frame = OPgetDirector()->getChannelManager()->getSample(context.getTime());

    duplicateSource(0, context);
    flags().timeDep = true;

    //    Reset _simulation if we have new object
    // OR frame changes to back
    if(is_objmesh_input_changed() || last_frame > frame)
        reset();

    // Load "fixed" attributes if frame changes to _back_ or not changed
    if(last_frame >= frame)
        load_fixed_vertices();
    else
    {
        _simulation->do_timestep();
        apply_cloth_deformations();
    }

    last_frame = frame;
    return error();
}
catch(const std::runtime_error &e)
{
    set_node_error(e.what());
    return error();
}
catch(...)
{
    set_node_error("An unknown error has occured");
    return error();
}

void Cloth::opChanged(OP_EventType reason, void *data)
{
    BaseSOP::opChanged(reason, data);
}

void Cloth::apply_cloth_deformations()
{
    auto mesh = _simulation->get_scene_object_deformable()->GetMesh();

    GA_Offset ptoff;
    Vec3d point_position;
    UT_Vector3 houdini_point_position;

    size_t i = 0;
    GA_FOR_ALL_PTOFF(gdp, ptoff)
    {
        point_position = mesh->getPosition(i);
        houdini_point_position.x() = point_position[0];
        houdini_point_position.y() = point_position[1];
        houdini_point_position.z() = point_position[2];

        gdp->setPos3(ptoff, houdini_point_position);
        ++i;
    }

    gdp->getP()->bumpDataId();
}

void Cloth::initialize_simulation()
{
    if(_simulation)
        _simulation->set_objmesh(get_objmesh());
    else
        _simulation = std::make_shared<ClothSimulation>(get_objmesh());
}

void Cloth::reset_simulation()
{
    _simulation.reset();
    initialize_simulation();
}