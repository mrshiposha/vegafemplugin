#ifndef PLUGIN_RANGED_PARAMETER_H
#define PLUGIN_RANGED_PARAMETER_H

#include "PluginParameter.h"
#include "PluginParameterRange.h"

class PluginRangedParameter : public PluginParameter
{
public:
    using PluginParameter::Node;
    using PluginParameter::Index;
    using PluginParameter::Time;
    using PluginParameter::Callback;

    PluginRangedParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        const PluginParameterRange &range
    );

    PluginRangedParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        const PluginParameterRange &range,
        fpreal default_value
    );

    PluginRangedParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        const PluginParameterRange &range,
        Callback
    );

    PluginRangedParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        const PluginParameterRange &range,
        fpreal default_value,
        Callback
    );

    PluginRangedParameter(const PluginRangedParameter &) = default;

    PluginRangedParameter(PluginRangedParameter &&) = default;

    ~PluginRangedParameter() = default;

    PluginRangedParameter &operator=(const PluginRangedParameter &) = default;

    PluginRangedParameter &operator=(PluginRangedParameter &&) = default;

    const PluginParameterRange &range() const;

    virtual PRM_Template build_prm_template() override;

private:
    const PluginParameterRange _range;
    PRM_Range                  _prm_range;
};

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range
);

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    fpreal default_value
);

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    PluginParameter::Callback
);

std::shared_ptr<PluginParameter> ranged_parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    const PluginParameterRange &range,
    fpreal default_value,
    PluginParameter::Callback
);

#endif // PLUGIN_RANGED_PARAMETER_H