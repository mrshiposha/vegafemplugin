#include "PluginParameters.h"

PluginParameters::PluginParameters()
: prm_templates(1, PRM_Template())
{}

PluginParameters::PluginParameters(const std::vector<std::shared_ptr<PluginParameter>> &parameter_ptrs)
: prm_templates(1, PRM_Template())
{
    for(auto &&parameter_ptr : parameter_ptrs)
        add(parameter_ptr);
}

PluginParameters::PluginParameters(std::initializer_list<std::shared_ptr<PluginParameter>> list)
: PluginParameters(std::vector<std::shared_ptr<PluginParameter>>(list.begin(), list.end()))
{}

PluginParameters &PluginParameters::add(std::shared_ptr<PluginParameter> parameter)
{
    parameters.push_back(parameter);
    prm_templates.insert(--prm_templates.end(), parameter->build_prm_template());
    return *this;
}

PRM_Template *PluginParameters::data()
{
    return prm_templates.data();
}

PluginParameters::operator PRM_Template*()
{
    return data();
}