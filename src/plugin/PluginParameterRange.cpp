#include "PluginParameterRange.h"

PluginParameterRange::Value::Value(fpreal value, PRM_RangeFlag flag)
: _value(value), _flag(flag)
{}

fpreal PluginParameterRange::Value::get() const
{
    return _value;
}

PRM_RangeFlag PluginParameterRange::Value::flag() const
{
    return _flag;
}

PluginParameterRange::Value::operator fpreal() const
{
    return get();
}

PluginParameterRange::PluginParameterRange(const Value &min, const Value &max)
: _min(min), _max(max)
{}

const PluginParameterRange::Value &PluginParameterRange::min() const
{
    return _min;
}

const PluginParameterRange::Value &PluginParameterRange::max() const
{
    return _max;
}

PluginParameterRange range(const PluginParameterRange::Value &min, const PluginParameterRange::Value &max)
{
    return PluginParameterRange(min, max);
}

PluginParameterRange::Value free(fpreal value)
{
    return PluginParameterRange::Value(value, PRM_RANGE_FREE);
}

PluginParameterRange::Value ui(fpreal value)
{
    return PluginParameterRange::Value(value, PRM_RANGE_UI);
}

PluginParameterRange::Value prm(fpreal value)
{
    return PluginParameterRange::Value(value, PRM_RANGE_PRM);
}

PluginParameterRange::Value restricted(fpreal value)
{
    return PluginParameterRange::Value(value, PRM_RANGE_RESTRICTED);
} 