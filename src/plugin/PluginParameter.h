#ifndef PLUGIN_PARAMETER_H
#define PLUGIN_PARAMETER_H

#include <stdexcept>
#include <memory>
#include <string>
#include <functional>

#include <PRM/PRM_Include.h>
#include <SOP/SOP_Node.h>

class PluginParameterNoDefaultValue : public std::runtime_error
{
public:
    PluginParameterNoDefaultValue()
    : std::runtime_error("Plugin parameter hasn't default value")
    {}
};

class PluginParameter
{
public:
    using Node     = void *;
    using Index    = int;
    using Time     = fpreal32;
    using Callback = int(*)(Node, Index, Time, const PRM_Template *);

    PluginParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label
    );

    PluginParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        fpreal default_value
    );

    PluginParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        Callback
    );

    PluginParameter
    (
        const PRM_Type &,
        const std::string &token,
        const std::string &label,
        fpreal default_value,
        Callback
    );

    PluginParameter(const PluginParameter &) = default;

    PluginParameter(PluginParameter &&) = default;

    PluginParameter &operator=(const PluginParameter &) = delete;

    PluginParameter &operator=(PluginParameter &&) = delete;

    virtual ~PluginParameter();

    const PRM_Type &type() const;

    const std::string &token() const;

    const std::string &label() const;

    bool has_default_value() const;

    const PRM_Default &default_value() const throw(PluginParameterNoDefaultValue);

    const std::shared_ptr<PRM_Default> default_value_ptr() const;

    virtual PRM_Template build_prm_template();

    operator PRM_Template();

    static fpreal get_float_value(SOP_Node *, Time, const PRM_Template *);

    static int get_int_value(SOP_Node *, Time, const PRM_Template *);

private:
    const PRM_Type                     prm_type;
    const std::string                  _token;
    const std::string                  _label;
    const std::shared_ptr<PRM_Default> prm_default;

protected:
    const Callback callback;
    PRM_Name prm_name;
};

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label
);

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    fpreal default_value
);

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    PluginParameter::Callback
);

std::shared_ptr<PluginParameter> parameter
(
    const PRM_Type &,
    const std::string &token,
    const std::string &label,
    fpreal default_value,
    PluginParameter::Callback
);

#endif // PLUGIN_PARAMETER_H