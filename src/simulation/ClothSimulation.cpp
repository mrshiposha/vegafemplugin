#include <algorithm>

#include "ClothSimulation.h"

static constexpr bool COMPUTE_STRETCH_SHEAR_FORCE     = true;
static constexpr bool COMPUTE_STRETCH_SHEAR_STIFFNESS = true;
static constexpr bool COPMUTE_BEND_FORCE              = true;
static constexpr bool COPMUTE_BEND_STIFFNESS          = true;

ClothSimulation::ClothSimulation(std::shared_ptr<ObjMesh> objmesh, ObjMeshPassing passing)
: deformable_object
  (
      std::make_shared<SceneObjectDeformable>
      (
          objmesh.get(), 
          /* deepCopy = */ passing == ObjMeshPassing::COPY
      )
  ),
  constrained_vertices(),
  clothbw_from_objmesh(new ClothBWFromObjMesh()),
  clothbw(nullptr),
  clothbw_stencil_force_model(nullptr),
  force_model_assembler(nullptr),
  force_model(nullptr),
  mass_matrix(nullptr),
  
  total_mass(0.),
  dofs_count(0),

  solver(nullptr),
  u(nullptr),
  velocity(nullptr),
  acceleration(nullptr),
  external_forces(nullptr),

  /* parameters */
  material
  ({
        8500.f,
        100.f,
        0.000f,
        0.000f,
        0.
  }),
  surface_density(0.25f),
  damping_mass(0.f),
  damping_stiffness(0.001f),
  add_gravity_to_elastic_forces(true),
  g(9.81f),
  use_rest_angles(false),
  timestep(0.033f),
  substeps(1),
  max_iterations(1),
  epsilon(1E-5),
  is_parameters_changed(true)
{
    parameters[0] = COMPUTE_STRETCH_SHEAR_FORCE;
    parameters[1] = COMPUTE_STRETCH_SHEAR_STIFFNESS;
    parameters[2] = COPMUTE_BEND_FORCE;
    parameters[3] = COPMUTE_BEND_STIFFNESS;
}

ClothSimulation::SolverCode ClothSimulation::do_timestep()
{
    if(is_parameters_changed)
    {
        initialize_solver();
        is_parameters_changed = false;
    }

    SolverCode code = 0;
    for(size_t i = 0; i < substeps; ++i)
    {
        code = solver->DoTimestep();

        if(code == 0)
        {
            std::memcpy(u.get(), solver->Getq(), sizeof(double) * dofs_count);
            std::memcpy(velocity.get(), solver->Getqvel(), sizeof(double) * dofs_count);
            std::memcpy(acceleration.get(), solver->Getqaccel(), sizeof(double) * dofs_count);

            deformable_object->BuildFaceNormals();
            deformable_object->SetVertexDeformations(u.get());
        }
        else
            return code;
    }

    return code;
}

bool ClothSimulation::is_vertex_fixed(VertexIndex index) const
{
    return constrained_vertices.count(index) != 0;
}

void ClothSimulation::fix_vertex(VertexIndex index)
{
    fix_vertex_to_position(index, get_vertex_position(index));
}

void ClothSimulation::unfix_all_vertices()
{
    constrained_vertices.clear();

    is_parameters_changed = true;
}

void ClothSimulation::unfix_vertex(VertexIndex index)
{
    constrained_vertices.erase(index);

    is_parameters_changed = true;
}

void ClothSimulation::fix_vertex_to_position(VertexIndex index, const Vector3d &position)
{
    constrained_vertices[index] = position;

    is_parameters_changed = true;
}

void ClothSimulation::set_objmesh(std::shared_ptr<ObjMesh> objmesh, ObjMeshPassing passing)
{
    deformable_object = std::make_shared<SceneObjectDeformable>
    (
        objmesh.get(), 
        /* deepCopy = */ passing == ObjMeshPassing::COPY
    );

    is_parameters_changed = true;
}

std::shared_ptr<SceneObjectDeformable> ClothSimulation::get_scene_object_deformable() const
{
    return deformable_object;
}

void ClothSimulation::set_vertex_position(VertexIndex index, const Vector3d &position)
{
    deformable_object->SetSingleVertexRestPosition(index, position.x(), position.y(), position.z());
}

Vector3d ClothSimulation::get_vertex_position(VertexIndex index) const
{
    double x = 0., y = 0., z = 0.;
    deformable_object->GetSingleVertexPositionFromBuffer(index, &x, &y, &z);

    return Vector3d(x, y, z);
}

const std::map<ClothSimulation::VertexIndex, Vector3d> &ClothSimulation::get_fixed_vertices() const
{
    return constrained_vertices;
}

void ClothSimulation::set_material(const ClothBW::MaterialGroup &value)
{
    material = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_material_tensile_stiffness(float value)
{
    material.tensileStiffness = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_material_shear_stiffness(float value)
{
    material.shearStiffness = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_material_bend_stiffness_u(float value)
{
    material.bendStiffnessU = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_material_bend_stiffness_v(float value)
{
    material.bendStiffnessV = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_material_damping(float value)
{
    material.damping = value;

    is_parameters_changed = true;
}


void ClothSimulation::set_surface_density(float value)
{
    surface_density = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_damping_mass(float value)
{
    damping_mass = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_damping_stiffness(float value)
{
    damping_stiffness = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_adding_gravity_to_elastic_forces(bool value)
{
    add_gravity_to_elastic_forces = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_g(float value)
{
    g = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_rest_angles_usage(bool value)
{
    use_rest_angles = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_timestep(float value)
{
    timestep = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_substeps(size_t value)
{
    substeps = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_max_iterations(size_t value)
{
    max_iterations = value;

    is_parameters_changed = true;
}

void ClothSimulation::set_epsilon(double value)
{
    epsilon = value;

    is_parameters_changed = true;
}


const ClothBW::MaterialGroup &ClothSimulation::get_material() const
{
    return material;
}

float ClothSimulation::get_material_tensile_stiffness() const
{
    return material.tensileStiffness;
}

float ClothSimulation::get_material_shear_stiffness() const
{
    return material.shearStiffness;
}

float ClothSimulation::get_material_bend_stiffness_u() const
{
    return material.bendStiffnessU;
}

float ClothSimulation::get_material_bend_stiffness_v() const
{
    return material.bendStiffnessV;
}

float ClothSimulation::get_material_damping() const
{
    return material.damping;
}


float ClothSimulation::get_surface_density() const
{
    return surface_density;
}

float ClothSimulation::get_damping_mass() const
{
    return damping_mass;
}

float ClothSimulation::get_damping_stiffness() const
{
    return damping_stiffness;
}

bool ClothSimulation::is_gravity_added_to_elastic_forces() const
{
    return add_gravity_to_elastic_forces;
}

float ClothSimulation::get_g() const 
{
    return g;
}

bool ClothSimulation::is_rest_angles_used() const
{
    return use_rest_angles;
}

float ClothSimulation::get_timestep() const
{
    return timestep;
}

size_t ClothSimulation::get_substeps() const
{
    return substeps;
}

size_t ClothSimulation::get_max_iterations() const
{
    return max_iterations;
}

double ClothSimulation::get_epsilon() const
{
    return epsilon;
}

void ClothSimulation::initialize_solver()
{
    clothbw.reset
    (
        clothbw_from_objmesh->GenerateClothBW
        (
            deformable_object->GetMesh(),
            surface_density,
            material,
            add_gravity_to_elastic_forces
        )
    );

    clothbw->SetGravity(add_gravity_to_elastic_forces, g);

    clothbw_stencil_force_model.reset(new ClothBWStencilForceModel(clothbw.get()));
    force_model_assembler.reset(new ForceModelAssembler(clothbw_stencil_force_model.get()));
    force_model = force_model_assembler.get();

    SparseMatrix *mass_matrix_ptr = nullptr;
    clothbw->GenerateMassMatrix(&mass_matrix_ptr);
    clothbw->SetComputationMode(parameters);
    clothbw->UseRestAnglesForBendingForces(use_rest_angles);

    mass_matrix.reset(mass_matrix_ptr);

    total_mass = mass_matrix->SumEntries() / 3.0;
    dofs_count = 3 * clothbw->GetNumVertices();

    solver.reset
    (
        new ImplicitBackwardEulerSparse
        (
            dofs_count,
            timestep / substeps,
            mass_matrix.get(),
            force_model,
            0,
            nullptr,
            damping_mass,
            damping_stiffness,
            max_iterations,
            epsilon
        )
    );

    u.reset(new double[dofs_count]);
    velocity.reset(new double[dofs_count]);
    acceleration.reset(new double[dofs_count]);
    external_forces.reset(new double[dofs_count]);

    std::memset(u.get(), 0, dofs_count*sizeof(double));
    std::memset(external_forces.get(), 0, dofs_count*sizeof(double));

    solver->SetState(u.get());
    solver->SetExternalForces(external_forces.get());

    deformable_object->BuildNeighboringStructure();
}