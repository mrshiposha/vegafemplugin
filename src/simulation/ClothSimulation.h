#ifndef SIMULATION_CLOTH_H
#define SIMULATION_CLOTH_H

#include <map>
#include <memory>
#include <objMesh.h>
#include <sceneObjectDeformable.h>
#include <clothBWFromObjMesh.h>
#include <clothBWStencilForceModel.h>
#include <forceModelAssembler.h>
#include <implicitBackwardEulerSparse.h>

#include "math/Vector3d.h"

class ClothSimulation
{
public: 
    using VertexIndex = int;
    using SolverCode  = int;

    enum class ObjMeshPassing
    {
        COPY,
        REF
    };

    ClothSimulation(std::shared_ptr<ObjMesh>, ObjMeshPassing = ObjMeshPassing::COPY);

    SolverCode do_timestep();

    bool is_vertex_fixed(VertexIndex) const;

    void fix_vertex(VertexIndex);

    void unfix_vertex(VertexIndex);

    void unfix_all_vertices();

    void fix_vertex_to_position(VertexIndex, const Vector3d &position);

    void set_objmesh(std::shared_ptr<ObjMesh>, ObjMeshPassing = ObjMeshPassing::COPY);

    std::shared_ptr<SceneObjectDeformable> get_scene_object_deformable() const;

    void set_vertex_position(VertexIndex, const Vector3d &);

    Vector3d get_vertex_position(VertexIndex) const;

    const std::map<VertexIndex, Vector3d> &get_fixed_vertices() const;
    

    /* parameters */
    void                          set_material(const ClothBW::MaterialGroup &);
    void                          set_material_tensile_stiffness(float);
    void                          set_material_shear_stiffness(float);
    void                          set_material_bend_stiffness_u(float);
    void                          set_material_bend_stiffness_v(float);
    void                          set_material_damping(float);
    void                          set_surface_density(float);
    void                          set_damping_mass(float);
    void                          set_damping_stiffness(float);
    void                          set_adding_gravity_to_elastic_forces(bool);
    void                          set_g(float);
    void                          set_rest_angles_usage(bool);
    void                          set_timestep(float);
    void                          set_substeps(size_t);
    void                          set_max_iterations(size_t);
    void                          set_epsilon(double);

    const ClothBW::MaterialGroup &get_material()                       const;
    float                         get_material_tensile_stiffness()     const;
    float                         get_material_shear_stiffness()       const;
    float                         get_material_bend_stiffness_u()      const;
    float                         get_material_bend_stiffness_v()      const;
    float                         get_material_damping()               const;
    float                         get_surface_density()                const;
    float                         get_damping_mass()                   const;
    float                         get_damping_stiffness()              const;
    bool                          is_gravity_added_to_elastic_forces() const;
    float                         get_g()                              const;
    bool                          is_rest_angles_used()                const;
    float                         get_timestep()                       const;
    size_t                        get_substeps()                       const;
    size_t                        get_max_iterations()                 const;
    double                        get_epsilon()                        const;

private:
    void initialize_solver();

    std::shared_ptr<SceneObjectDeformable> deformable_object;
    std::map<VertexIndex, Vector3d>        constrained_vertices;

    std::unique_ptr<ClothBWFromObjMesh>       clothbw_from_objmesh;
    std::unique_ptr<ClothBW>                  clothbw;
    std::unique_ptr<ClothBWStencilForceModel> clothbw_stencil_force_model;
    std::unique_ptr<ForceModelAssembler>      force_model_assembler;
    ForceModel                               *force_model;
    std::unique_ptr<SparseMatrix>             mass_matrix;

    double total_mass;
    size_t dofs_count;
    std::unique_ptr<ImplicitBackwardEulerSparse> solver;
    std::unique_ptr<double[]>                    u;
    std::unique_ptr<double[]>                    velocity;
    std::unique_ptr<double[]>                    acceleration;
    std::unique_ptr<double[]>                    external_forces;

    /* parameters */
    ClothBW::MaterialGroup material;
    float                  surface_density;
    float                  damping_mass;
    float                  damping_stiffness;
    bool                   add_gravity_to_elastic_forces;
    float                  g;
    bool                   use_rest_angles;
    float                  timestep;
    size_t                 substeps;
    size_t                 max_iterations;
    double                 epsilon;

    bool parameters[4];

    bool is_parameters_changed;
};

#endif // SIMULATION_CLOTH_H
